import numpy as np
from matplotlib import pyplot as plt
from math import pi

u=1 #x-posición del centro
v=0.5 #y-posición del centro
a=2 #radio en x-ax
b=1.5 #radio en y-ax

t=np.linspace(0, 2*pi,100)
plt.plot(u+a*np.cos(t) , v+b*np.sin(t))
plt.grid(color="lightgray", linestyle="--")
plt.show()
